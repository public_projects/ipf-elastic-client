@echo off
set SPRING_PROFILES_ACTIVE=LOCAL
echo Setting JAVA_HOME
set JAVA_HOME=C:\software\Java\jdk-17.0.6
echo setting PATH
set PATH=C:\software\Java\jdk-17.0.6\bin;%PATH%
echo Display java version
java -version
call mvn clean install -DskipTests
pause
