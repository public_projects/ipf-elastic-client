package org.green.elastic.ipf.client.entity;

import java.util.List;

public class Student {
    private String name;
    private int age;
    private String major;
    private double gpa;
    private List<String> courses;
    private List<String> extracurriculars;
    private List<String> honors;

    public Student(String name, int age, String major, double gpa, List<String> courses, List<String> extracurriculars, List<String> honors) {
        this.name = name;
        this.age = age;
        this.major = major;
        this.gpa = gpa;
        this.courses = courses;
        this.extracurriculars = extracurriculars;
        this.honors = honors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public List<String> getExtracurriculars() {
        return extracurriculars;
    }

    public void setExtracurriculars(List<String> extracurriculars) {
        this.extracurriculars = extracurriculars;
    }

    public List<String> getHonors() {
        return honors;
    }

    public void setHonors(List<String> honors) {
        this.honors = honors;
    }
}
