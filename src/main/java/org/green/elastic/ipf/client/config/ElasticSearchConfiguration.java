package org.green.elastic.ipf.client.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.green.elastic.ipf.client.config.property.ElasticConfigurationParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class ElasticSearchConfiguration {

    @Autowired
    private ElasticConfigurationParameter elasticConfigurationParameter;

    @Primary
    @Bean(name = "restClient", destroyMethod = "close")
    public RestClient getRestClient() {
        return RestClient.builder(new HttpHost(elasticConfigurationParameter.getIp(), elasticConfigurationParameter.getPort(), "http")).build();
    }

    @Bean(name = "highLevelClient",destroyMethod = "close")
    public RestHighLevelClient getRestHighLevelClient() {
        return new RestHighLevelClient(RestClient.builder(new HttpHost(elasticConfigurationParameter.getIp(), elasticConfigurationParameter.getPort(), "http")));
    }
}
