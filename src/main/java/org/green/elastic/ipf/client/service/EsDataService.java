package org.green.elastic.ipf.client.service;

import com.google.gson.Gson;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.green.elastic.ipf.client.entity.CaRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EsDataService {
    @Autowired
    private RestHighLevelClient highLevelClient;

    public List<String> getCustomData(List<QueryBuilder> mustQueryBuilders) throws IOException {
        Gson gson = new Gson();
        SearchRequest searchRequest = new SearchRequest("convoy-*");
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(20);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        for (QueryBuilder queryBuilder : mustQueryBuilders) {
            boolQueryBuilder.must(queryBuilder);
        }

        searchSourceBuilder.query(boolQueryBuilder);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = highLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        System.err.println(searchResponse.getScrollId());
        SearchHits searchHits = searchResponse.getHits();
        SearchHit[] hits = searchHits.getHits();
        long hitSize = searchHits.getTotalHits();
        System.err.println("Total records: " + hitSize);

        int counter = 1;
        List<CaRecord> caRecordList = new ArrayList<>();
        List<String> docIds = new ArrayList<>();
        for (SearchHit hit : hits) {
            System.err.println(counter + ": " + hit.getId() + ": " + hit.getSourceAsString());

            CaRecord caRecord = gson.fromJson(hit.getSourceAsString(), CaRecord.class);
            caRecordList.add(caRecord);
            docIds.add(hit.getId());
            counter++;
        }

        getOnScrollableData(gson, searchResponse.getScrollId(), counter, true, caRecordList, docIds);
        System.err.println();
        System.err.println();
        System.err.println(CaRecord.HEADERS);

        counter = 0;
        for (CaRecord record : caRecordList) {
            System.err.println(counter + "," + record.toString());
            counter++;
        }
        return docIds;
    }

    private void getOnScrollableData(Gson gson,
                                     String scrollId,
                                     int counter,
                                     boolean hasData,
                                     List<CaRecord> caRecordList,
                                     List<String> docIds) throws IOException {
        if (hasData == false) return;
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
        scrollRequest.scroll(TimeValue.timeValueSeconds(30));
        SearchResponse searchScrollResponse = highLevelClient.scroll(scrollRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchScrollResponse.getHits();
        hasData = false;

        for (SearchHit hit : hits) {
            System.err.println(counter + ": " + hit.getId() + ": " + hit.getSourceAsString());
            CaRecord caRecord = gson.fromJson(hit.getSourceAsString(), CaRecord.class);
            caRecordList.add(caRecord);
            docIds.add(hit.getId());
            counter++;
            hasData = true;
        }
        getOnScrollableData(gson, searchScrollResponse.getScrollId(), counter, hasData, caRecordList, docIds);
    }
}
