package org.green.elastic.ipf.client.entity;

public class Cell {
    private CellGlobalId cellGlobalId;
    private Location location;
    private CellMetadata cellMetadata;

    public CellGlobalId getCellGlobalId() {
        return cellGlobalId;
    }

    public void setCellGlobalId(CellGlobalId cellGlobalId) {
        this.cellGlobalId = cellGlobalId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public CellMetadata getCellMetadata() {
        return cellMetadata;
    }

    public void setCellMetadata(CellMetadata cellMetadata) {
        this.cellMetadata = cellMetadata;
    }

    public static class CellMetadata {
        private long usageTimestamp;
        private String actualCgi;

        public long getUsageTimestamp() {
            return usageTimestamp;
        }

        public void setUsageTimestamp(long usageTimestamp) {
            this.usageTimestamp = usageTimestamp;
        }

        public String getActualCgi() {
            return actualCgi;
        }

        public void setActualCgi(String actualCgi) {
            this.actualCgi = actualCgi;
        }
    }


}
