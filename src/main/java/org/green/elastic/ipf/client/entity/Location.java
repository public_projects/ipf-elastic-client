package org.green.elastic.ipf.client.entity;

public class Location {
    private LocationCoordinate location;
    private LocationMetadata locationMetadata;

    public Location(){}

    public Location(LocationCoordinate location, LocationMetadata locationMetadata) {
        this.location = location;
        this.locationMetadata = locationMetadata;
    }

    public LocationCoordinate getLocation() {
        return location;
    }

    public void setLocation(LocationCoordinate location) {
        this.location = location;
    }

    public LocationMetadata getLocationMetadata() {
        return locationMetadata;
    }

    public void setLocationMetadata(LocationMetadata locationMetadata) {
        this.locationMetadata = locationMetadata;
    }

    public static class LocationMetadata {
        private String provider;
        private String subject;

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }
    }

    public static class LocationCoordinate {
        private double lat;
        private double lon;

        public LocationCoordinate(){}

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }
    }
}
