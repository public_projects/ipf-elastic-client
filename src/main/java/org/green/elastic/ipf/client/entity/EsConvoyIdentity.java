package org.green.elastic.ipf.client.entity;

import java.io.Serializable;
import java.util.Objects;

public class EsConvoyIdentity implements Serializable {
    private String id;
    private String type;
    private String service;

    public EsConvoyIdentity(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EsConvoyIdentity that = (EsConvoyIdentity) o;
        return Objects.equals(id, that.id) && Objects.equals(type, that.type) && Objects.equals(service, that.service);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, service);
    }

    @Override
    public String toString() {
        return id + "," + type +  "," + service;
    }
}
