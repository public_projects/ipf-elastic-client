package org.green.elastic.ipf.client.config.mapping;

public class TemplateMapping {

    /**
     *
     * {
     *   "template" : "*",
     *   "settings" : {
     *     "number_of_shards" : 5,
     *     "number_of_replicas" : 1
     *   },
     *   "mappings" : {
     *     "_default_" : {
     *       "dynamic_templates" : [ {
     *         "string_fields" : {
     *           "match" : "*",
     *           "match_mapping_type" : "string",
     *           "mapping" : {
     *             "type" : "text",
     *             "index" : "analyzed",
     *             "omit_norms" : true,
     *             "fields" : {
     *               "keyword" : {
     *                 "type": "keyword"
     *               }
     *             }
     *           }
     *         }
     *       } ],
     *       "properties" : {
     *         "@timestamp": {
     *           "type":   "date",
     *           "format": "yyyy-MM-dd HH:mm:ss"
     *         }
     *       }
     *     }
     *   }
     * }
     *
     */
    public static String mapping1 = "{\n" +
            "  \"template\" : \"*\",\n" +
            "  \"settings\" : {\n" +
            "    \"number_of_shards\" : 5,\n" +
            "    \"number_of_replicas\" : 1\n" +
            "  },\n" +
            "  \"mappings\" : {\n" +
            "    \"_default_\" : {\n" +
            "      \"dynamic_templates\" : [ {\n" +
            "        \"string_fields\" : {\n" +
            "          \"match\" : \"*\",\n" +
            "          \"match_mapping_type\" : \"string\",\n" +
            "          \"mapping\" : {\n" +
            "            \"type\" : \"text\",\n" +
            "            \"index\" : \"analyzed\",\n" +
            "            \"omit_norms\" : true,\n" +
            "            \"fields\" : {\n" +
            "              \"keyword\" : {\n" +
            "                \"type\": \"keyword\"\n" +
            "              }\n" +
            "            }\n" +
            "          }\n" +
            "        }\n" +
            "      } ],\n" +
            "      \"properties\" : {\n" +
            "        \"@timestamp\": {\n" +
            "          \"type\":   \"date\",\n" +
            "          \"format\": \"yyyy-MM-dd HH:mm:ss\"\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    /**
     * {
     *   "mappings": {
     *     "properties": {
     *       "identityType": {
     *         "type": "keyword"
     *       },
     *       "identityId": {
     *         "type": "keyword"
     *       },
     *       "location": {
     *         "type": "geo_point"
     *       },
     *       "time": {
     *         "type": "date",
     *         "format": "epoch_millis"
     *       }
     *     }
     *   }
     * }
     */
    public static String mapping2 = "{\n" +
            "  \"mappings\": {\n" +
            "    \"properties\": {\n" +
            "      \"identityType\": {\n" +
            "        \"type\": \"keyword\"\n" +
            "      },\n" +
            "      \"identityId\": {\n" +
            "        \"type\": \"keyword\"\n" +
            "      },\n" +
            "      \"location\": {\n" +
            "        \"type\": \"geo_point\"\n" +
            "      },\n" +
            "      \"time\": {\n" +
            "        \"type\": \"date\",\n" +
            "        \"format\": \"epoch_millis\"\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";
}
