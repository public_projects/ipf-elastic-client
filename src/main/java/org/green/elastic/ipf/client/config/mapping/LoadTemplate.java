package org.green.elastic.ipf.client.config.mapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class LoadTemplate {
    private Logger log = LoggerFactory.getLogger(LoadTemplate.class);

    @Autowired
    private ResourceLoader resourceLoader;

    public String getFileContent(String fileName) {
        try {
            Resource resource = resourceLoader.getResource("classpath:" + fileName);
            File file = resource.getFile();
            log.info("File absolute path : {} " + file.getAbsolutePath());
            Path path = Paths.get(file.getAbsolutePath());
            byte[] bytes = Files.readAllBytes(path);
            String content = new String(bytes);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(1);
        return null;
    }
}
