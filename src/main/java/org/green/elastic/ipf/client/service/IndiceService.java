package org.green.elastic.ipf.client.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.RequestLine;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.ResponseException;
import org.elasticsearch.client.ResponseListener;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.green.elastic.ipf.client.entity.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.UUID;

@Service
public class IndiceService {
    private final Logger log = LoggerFactory.getLogger(IndiceService.class);

    private ObjectMapper objectMapper;

    @Autowired
    private RestClient restClient;

    @Autowired
    private RestHighLevelClient highLevelClient;

    public IndiceService() {
        objectMapper = new ObjectMapper();
    }

    public void deleteDoc(String indexName, String id) throws IOException {
        Request request = new Request("DELETE",  indexName + "/" +"doc"+ "/" + id);
        request.addParameter("refresh", "true");
        request.setJsonEntity("");

       restClient.performRequestAsync(request, new ResponseListener() {
            @Override
            public void onSuccess(Response response) {
            }

            @Override
            public void onFailure(Exception e) {
                System.err.println("Failed to delete!");
            }
        });
    }

    public String getAllIndices() throws IOException {
        Request request = new Request("GET", "/_cat/indices?h=i");
        request.addParameter("pretty", "true");
        Response response = restClient.performRequest(request);
        RequestLine requestLine = response.getRequestLine();
        HttpHost host = response.getHost();
        int statusCode = response.getStatusLine().getStatusCode();
        Header[] headers = response.getHeaders();
        return EntityUtils.toString(response.getEntity());
    }

    public void getIndex(String indexName) throws IOException {
        Request request = new Request("GET", indexName);
        request.addParameter("pretty", "true");
        Response response = restClient.performRequest(request);
        String responseBody = EntityUtils.toString(response.getEntity());
        log.info(responseBody);
    }

    public boolean indexExist(String indexName) throws IOException {
        Request request = new Request("GET", indexName);
        try {
            restClient.performRequest(request);
        } catch (ResponseException exception) {
            System.err.println(exception.getMessage());
            return false;
        }
        return true;
    }

    public void getAll(String indexName, int size) throws IOException {
        Request request = new Request("GET", indexName + "/_search?_source=true");
        request.addParameter("pretty", "true");
        Response response = restClient.performRequest(request);
        String responseBody = EntityUtils.toString(response.getEntity());
        log.info(responseBody);
    }

    public void getClientInfo() throws IOException {
        Request request = new Request(
                "GET",
                "/");
        request.addParameter("pretty", "true");
        Response response = restClient.performRequest(request);
        RequestLine requestLine = response.getRequestLine();
        HttpHost host = response.getHost();
        int statusCode = response.getStatusLine().getStatusCode();
        Header[] headers = response.getHeaders();
        String responseBody = EntityUtils.toString(response.getEntity());

        log.info(responseBody);
    }

    public void createIndex(String indexName, String mapping) throws IOException  {
        Request request = new Request("PUT", indexName);
        request.setJsonEntity(mapping);
        restClient.performRequest(request);
    }

    public int addDocument(String indexName, String id, String dataJson) throws IOException {
        Request request = new Request("PUT", indexName + "/" + "_doc" +"/"+ id);

        request.setJsonEntity(dataJson);
        System.err.println(request);

        Response response = restClient.performRequest(request);
        return response.getStatusLine().getStatusCode();
    }

    public int addStudent(String indexName, UUID id, Student student) throws IOException {
        Request request = new Request("PUT", indexName + "/" + "_doc" +"/"+ id);

        request.setJsonEntity(objectMapper.writeValueAsString(student));
        System.err.println(request);

        Response response = restClient.performRequest(request);
        return response.getStatusLine().getStatusCode();
    }

    public void getDocumentById(String indexName, String id, boolean sourceOnly) throws Exception {
        String additionalParam = "";
        if (sourceOnly) additionalParam = "/_source"; // _source is the actual data stored.
        Request request = new Request(
                "GET",
                indexName + "/_doc/"
                        + id
                        + additionalParam
        );

        Response response = restClient.performRequest(request);
        String content = EntityUtils.toString(response.getEntity());
        System.out.println(content);
    }

    public int dropIndex(String indexName) throws Exception {
        Request request = new Request("DELETE", indexName);
        System.err.println(request);
        Response response = restClient.performRequest(request);
        System.err.println(indexName + ": Dropped!");
        return response.getStatusLine().getStatusCode();
    }
}
