package org.green.elastic.ipf.client.entity;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class CaRecord {
    public static final DateTimeFormatter dateReadableFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
    public static final String HEADERS = "no,docId,InvestigationId,start,formatedStart,cgi,latitude,longitude,geoHash,inception,provider,subject,id,type,service";
    private String investigationId;
    private long start;
    private EsConvoyIdentity esConvoyIdentity;
    private String geoHash;
    private String inception;
    private String cgi;
    private Location location;

    public CaRecord() {
        // default constructor
    }

    public String getInvestigationId() {
        return investigationId;
    }

    public void setInvestigationId(String investigationId) {
        this.investigationId = investigationId;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public String getGeoHash() {
        return geoHash;
    }

    public void setGeoHash(String geoHash) {
        this.geoHash = geoHash;
    }

    public Location getLocation() {
        return location;
    }

    public String getCgi() {
        return cgi;
    }

    public void setCgi(String cgi) {
        this.cgi = cgi;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getInception() {
        return inception;
    }

    public void setInception(String inception) {
        this.inception = inception;
    }

    public EsConvoyIdentity getEsConvoyIdentity() {
        return esConvoyIdentity;
    }

    public void setEsConvoyIdentity(EsConvoyIdentity esConvoyIdentity) {
        this.esConvoyIdentity = esConvoyIdentity;
    }

    @Override
    public String toString() {
        String dateTime = dateReadableFormatter.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(start), ZoneOffset.UTC));

        return investigationId + ","
                + start + ","
                + dateTime + ","
                + cgi + ","
                + location.getLocation().getLat() + ","
                + location.getLocation().getLon() + ","
                + geoHash + ","
                + inception + ","
                + location.getLocationMetadata().getProvider() + ","
                + location.getLocationMetadata().getSubject()+ ","
                + esConvoyIdentity.toString();
    }
}
