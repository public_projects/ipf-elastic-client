package org.green.elastic.ipf.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfElasticClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfElasticClientApplication.class, args);
	}
}
