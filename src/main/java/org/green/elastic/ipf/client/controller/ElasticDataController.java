package org.green.elastic.ipf.client.controller;

import org.green.elastic.ipf.client.config.property.ElasticConfigurationParameter;
import org.green.elastic.ipf.client.service.IndiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ElasticDataController {
    @Autowired
    private IndiceService indiceService;

    @CrossOrigin
    @GetMapping(value="/get/all/indices")
    public ResponseEntity<Map<String, String>> getAllIndices() throws Exception {
        Map<String, String> response = new HashMap<>();
        response.put("indices", indiceService.getAllIndices());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
