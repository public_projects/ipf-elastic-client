package org.green.elastic.ipf.client;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.delete.DeleteRequest;
import com.google.gson.Gson;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.green.elastic.ipf.client.config.mapping.LoadTemplate;
import org.green.elastic.ipf.client.entity.CaRecord;
import org.green.elastic.ipf.client.entity.Student;
import org.green.elastic.ipf.client.service.IndiceService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@ActiveProfiles("DCNG2")
//@ActiveProfiles("LOCAL")
@SpringBootTest
class IpfElasticClientApplicationTests {
    private final Logger log = LoggerFactory.getLogger(IpfElasticClientApplicationTests.class);
//    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS").withZone(ZoneOffset.UTC);
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneOffset.UTC);

    public static final String ES_IDENTITY = "esConvoyIdentity";
    public static final String ES_IDENTITY_ID = "esConvoyIdentity.id";
    public static final String ES_IDENTITY_TYPE = "esConvoyIdentity.type";
    public static final String ES_IDENTITY_SERVICE = "esConvoyIdentity.service";

    @Autowired
    private IndiceService indiceService;

    @Autowired
    private RestHighLevelClient highLevelClient;

    @Autowired
    private LoadTemplate loadTemplate;

    private String testIndexName = "student";

    @Test
    void getAllIndices() throws IOException {
        log.info(indiceService.getAllIndices());
    }

    @Test
    public void getInfo() throws IOException {
        indiceService.getClientInfo();
    }

    @Test
    public void get() throws IOException {
//		indiceService.getAll("auditlog-2023.03", 6);
//        indiceService.getAll("auditlog-2023.03", 30);
//        indiceService.getAll(testIndexName, 30);

        indiceService.getIndex("ca-record");
    }

    @Test
    public void process() throws Exception {
        String indexName = "activityrecord";
        boolean indexExist = indiceService.indexExist(indexName);

        if (indexExist) {
            System.err.println("index: [" + indexName + "] already exist!");
            indiceService.dropIndex(indexName); // Drop index
            System.err.println("Rerun to recreate index with data!");
            return;
        }

        // Create index
        String json = loadTemplate.getFileContent("/elasticsearch/templates/activityrecord_work.json");
        indiceService.createIndex(indexName, json);

        // insert data 1
        json = loadTemplate.getFileContent("/elasticsearch/data/activityrecord-data-1.json");
        String id1 = UUID.randomUUID().toString();
        indiceService.addDocument(indexName, id1, json);

        // insert data 2
        json = loadTemplate.getFileContent("/elasticsearch/data/activityrecord-data-2.json");
        String id2 = UUID.randomUUID().toString();
        indiceService.addDocument(indexName, id2, json);

        // insert data 3 (same data 2)
        json = loadTemplate.getFileContent("/elasticsearch/data/activityrecord-data-2.json");
        String id3 = UUID.randomUUID().toString();
        indiceService.addDocument(indexName, id3, json);

        // Get data 1
        indiceService.getDocumentById(indexName, id1, true);

        // Get data 2
        indiceService.getDocumentById(indexName, id2, true);

        // Get data 3
        indiceService.getDocumentById(indexName, id3, true);
    }

    @Test
    public void getAll() throws Exception {
        indiceService.getAll("ca-record", 10);
    }

    @Test
    public void dropIndex() throws Exception {
        indiceService.dropIndex("ca-record");
    }

    @Test
    public void createIndex() throws IOException {
        String json = loadTemplate.getFileContent("/elasticsearch/templates/convoy.json");
        indiceService.createIndex("convoy", json);
    }

    @Test
    public void addDocument() throws IOException {
        String json = loadTemplate.getFileContent("/elasticsearch/data/activityrecord-data-1.json");
        indiceService.addDocument("activityrecord", UUID.randomUUID().toString(), json);
    }

    @Test
    public void addDocumentStudent() throws Exception {

        Student student1 = new Student("Tom Jones", 25, "Computer Science", 3.5,
                                       Arrays.asList("Calculus", "Discrete Mathematics", "Data Structures", "Algorithms"),
                                       Arrays.asList("Chess Club", "Robotics Club", "Programming Club"),
                                       Arrays.asList("Dean's List", "Award for Academic Excellence"));
        indiceService.addStudent(testIndexName, UUID.randomUUID(), student1);

        Student student2 = new Student("Mary Smith", 22, "Business", 3.7,
                                       Arrays.asList("Economics", "Finance", "Accounting", "Marketing"),
                                       Arrays.asList("Debate Club", "Fashion Club", "Volunteering"),
                                       Arrays.asList("Honors", "Award for Academic Excellence"));
        indiceService.addStudent(testIndexName, UUID.randomUUID(), student2);

        Student student3 = new Student("James Smith", 19, "Computer Science", 3.6,
                                       Arrays.asList("Calculus", "Computer Networks", "Database Systems", "Programming Languages"),
                                       Arrays.asList("Robotics Club", "Chess Club", "Programming Club"),
                                       Arrays.asList("Honors", "Award for Academic Excellence"));
        indiceService.addStudent(testIndexName, UUID.randomUUID(), student3);

        Student student4 = new Student("Emily Johnson", 21, "Business", 3.8,
                                       Arrays.asList("Economics", "Finance", "Accounting", "Marketing"),
                                       Arrays.asList("Volunteering", "Debate Club", "Fashion Club"),
                                       Arrays.asList("Dean's List", "Highest Honors"));

        indiceService.addStudent(testIndexName, UUID.randomUUID(), student4);
    }

    @Test
    public void getByIdentityId() throws IOException {
        SearchRequest searchRequest = new SearchRequest("ca-record");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("identityType", "imsi");
        searchSourceBuilder.query(matchQueryBuilder);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = highLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits searchHits = searchResponse.getHits();

        for (SearchHit hit : searchHits.getHits()) {
            Map<String, Object> source = hit.getSourceAsMap();
            System.err.println(source);
        }
    }

    @Test
    public void getBetweenDates() throws Exception {
        SearchRequest searchRequest = new SearchRequest("ca-record");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("investigationId", "d0812400-d9ac-11ed-9067-2c0da7f63000"));
        searchSourceBuilder.query(QueryBuilders.matchQuery("identityType", "imsi"));
        searchSourceBuilder.query(QueryBuilders.rangeQuery("start").gte(1680872400000l).lte(1680872400000l));
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = highLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits searchHits = searchResponse.getHits();

        for (SearchHit hit : searchHits.getHits()) {
            Map<String, Object> source = hit.getSourceAsMap();
            System.err.println(source);
        }
    }

    /**
     * Only returns 10 record! Why???? Not getting all records
     *
     * @throws Exception
     */
    @Test
    public void getMustMatchMixedQueries() throws Exception {
        QueryBuilder investigationIdQuery = QueryBuilders.termQuery("investigationId", "acf2c880-05d1-11ee-abf2-2c0da7f630d4");
        QueryBuilder identityTypeQuery = QueryBuilders.termQuery("identityType", "msisdn");
        QueryBuilder identityIdQuery = QueryBuilders.termQuery("identityId", "604022788630463");
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("start")
                                                           .gte(1680786000000l)
                                                           .lte(1680786000000l);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder
                .must(investigationIdQuery);
//                .must(identityTypeQuery)
//                .must(identityIdQuery)
//                .must(rangeQueryBuilder);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder);

        SearchRequest searchRequest = new SearchRequest("ca-record");
        searchRequest.source(searchSourceBuilder);

        System.err.println(searchRequest.source().toString());

        SearchResponse searchResponse = highLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits searchHits = searchResponse.getHits();

        for (SearchHit hit : searchHits.getHits()) {
            Map<String, Object> source = hit.getSourceAsMap();
            System.err.println(source);
        }
    }

    @Test
    public void getDocumentById() throws Exception {
        indiceService.getDocumentById(testIndexName, "6b56a608-b4e3-4a44-a726-1c2e97d301d2", true);
    }

    @Test
    public void getResource() {
        loadTemplate.getFileContent("/elasticsearch/templates/template2_work.json");
    }

    @Test
    void createDataUsingHighLevelClient() throws IOException {
        String index = "ca-record";
        String type = "doc";
        String id = UUID.randomUUID().toString();
        String json = loadTemplate.getFileContent("/elasticsearch/data/ca_record2.json");

        IndexRequest indexRequest = new IndexRequest(index, type, id);
        indexRequest.source(json, XContentType.JSON);

        try {
            IndexResponse indexResponse = highLevelClient.index(indexRequest, RequestOptions.DEFAULT);
            log.info("Index response:{}", indexResponse.getResult().toString());
        } catch (IOException e) {
            throw new RuntimeException("Error during create", e);
        }
    }

    @Test
    public void buildQuery() throws Exception {
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        boolQuery.must(QueryBuilders.termsQuery("investigationId", "9dbf2210-3050-11ee-9851-0242ac11000d"));

        BoolQueryBuilder nestedBoolQuery = QueryBuilders.boolQuery();
        nestedBoolQuery.must(QueryBuilders.termQuery(ES_IDENTITY_TYPE, "msisdn"));
        nestedBoolQuery.must(QueryBuilders.termQuery(ES_IDENTITY_ID, "8759026230701001"));

        NestedQueryBuilder nestedQueryBuilder = QueryBuilders.nestedQuery(ES_IDENTITY, nestedBoolQuery, ScoreMode.Avg);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.termsQuery("investigationId", "9dbf2210-3050-11ee-9851-0242ac11000d"));
//        boolQueryBuilder.must(QueryBuilders.rangeQuery(START).gte(from).lte(to));
        boolQueryBuilder.must(nestedQueryBuilder);

        List<String> documentIds = getCustomData(boolQueryBuilder);
    }

    public List<String> getCustomData(BoolQueryBuilder boolQueryBuilder) throws IOException {
        Gson gson = new Gson();
        SearchRequest searchRequest = new SearchRequest("convoy-*");
        searchRequest.scroll(TimeValue.timeValueMinutes(1L));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(20);

        searchSourceBuilder.query(boolQueryBuilder);
        System.err.println(searchSourceBuilder);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = highLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        System.err.println(searchResponse.getScrollId());
        SearchHits searchHits = searchResponse.getHits();
        SearchHit[] hits = searchHits.getHits();
        long hitSize = searchHits.getTotalHits();
        System.err.println("Total records: " + hitSize);

        int counter = 1;
        List<CaRecord> caRecordList = new ArrayList<>();
        List<String> docIds = new ArrayList<>();
        for (SearchHit hit : hits) {
            System.err.println(counter + ": " + hit.getId() + ": " + hit.getSourceAsString());

            CaRecord caRecord = gson.fromJson(hit.getSourceAsString(), CaRecord.class);
            caRecordList.add(caRecord);
            docIds.add(hit.getId());
            counter++;
        }

        getOnScrollableData(gson, searchResponse.getScrollId(), counter, true, caRecordList, docIds);
        System.err.println();
        System.err.println();
        System.err.println(CaRecord.HEADERS);

        counter = 0;
        for (CaRecord record : caRecordList) {
            System.err.println(counter + "," + record.toString());
            counter++;
        }
        return docIds;
    }

    private void getOnScrollableData(Gson gson,
                                     String scrollId,
                                     int counter,
                                     boolean hasData,
                                     List<CaRecord> caRecordList,
                                     List<String> docIds) throws IOException {
        if (hasData == false) return;
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
        scrollRequest.scroll(TimeValue.timeValueSeconds(30));
        SearchResponse searchScrollResponse = highLevelClient.scroll(scrollRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchScrollResponse.getHits();
        hasData = false;

        for (SearchHit hit : hits) {
            System.err.println(counter + ": " + hit.getId() + ": " + hit.getSourceAsString());
            CaRecord caRecord = gson.fromJson(hit.getSourceAsString(), CaRecord.class);
            caRecordList.add(caRecord);
            docIds.add(hit.getId());
            counter++;
            hasData = true;
        }
        getOnScrollableData(gson, searchScrollResponse.getScrollId(), counter, hasData, caRecordList, docIds);
    }

    @Test
    public void deleteData() throws Exception {
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        List<String> investigationIds = Collections.singletonList("1f608af0-141a-11ee-b946-2c0da7f630d4");
//        queryBuilders.add(QueryBuilders.matchQuery("investigationId", "76330ab0-1569-11ee-8397-2c0da7f630d4"));
        boolQuery.must(QueryBuilders.termsQuery("investigationId", "9dbf2210-3050-11ee-9851-0242ac11000d"));
//        queryBuilders.add(QueryBuilders.termsQuery("investigationId", investigationIds));
//        queryBuilders.add(QueryBuilders.matchQuery("investigationId", Collections.singleton("1f608af0-141a-11ee-b946-2c0da7f630d4")));
//        boolQuery.must(QueryBuilders.termsQuery("encodedId.keyword", "006Smsisdn015S244502168316128"));
//        boolQuery.must(QueryBuilders.rangeQuery("start").gte(1515081600000L).lte(1687967999999L));
        List<String> documentIds = getCustomData(boolQuery);
        System.err.println(documentIds);

        for (String docId : documentIds) {
            DeleteRequest deleteRequest = new DeleteRequest("convoy", "doc", docId);
//            highLevelClient.delete(deleteRequest, RequestOptions.DEFAULT); // Works too!
//            indiceService.deleteDoc("ca-record", docId); // This only partially deletes.
        }
    }

    @Test
    public void getDateSequence() {

        // Specify the desired date and time components
        int year = 2023;
        int month = 05;
        int day = 3;
        int hour = 22;
        int minute = 0;
        int second = 0;

        // Create a LocalDateTime object with the specified date and time
        LocalDateTime dateTime = LocalDateTime.of(year, month, day, hour, minute, second);
        dateTime.plusHours(1);

        for (int i = 0; i < 40; i++) {
            dateTime = dateTime.plusHours(1).plusMinutes(1);
            System.err.println(dateTimeFormatter.format(dateTime));
        }
    }
}
